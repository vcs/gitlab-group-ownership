#
FROM python:3.6-alpine
#
MAINTAINER Daniel Juarez <djuarezg@cern.ch>

#
# Copy scripts
#
COPY group_ownership.py /
COPY upload_log_s3.py /

#
# Python modules
#
COPY requirements.txt /

RUN pip install -r requirements.txt
