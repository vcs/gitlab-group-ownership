# Project and ownership retrieval

This project takes care of retrieving the complete ownership information to serve as a backup in case we detect discordances, specially when reported on SNow tickets.

It prints logs so they can be retrieved by [central monitoring](https://monit-timber-openshift.cern.ch/kibana/app/kibana#/discover?_g=()) and it also stores the information into `gitlaboperations(-dev)` S3 bucket.

