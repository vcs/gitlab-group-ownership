import json
import os
import re
import time
import requests
import traceback
import sys
import argparse
import logging
from botocore.client import Config
parser = argparse.ArgumentParser()
parser.add_argument("-u", "--base_url", required=True)
parser.add_argument("-t", "--admin_token", required=True)
args = parser.parse_args()

# Force logger to print through stderr and add default format to messages
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stderr)
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

# Global variables
api_sudo_token = {"Private-Token": args.admin_token}
base_url = args.base_url

# MAIN

# Retrieving the first 100 groups, just to get the total of pages. 100 is the max per page
resp = requests.get(base_url+"/groups?per_page=100", headers=api_sudo_token)
# Total pages using 100 per page
total_pages = resp.headers['X-Total-Pages']
# Where we will store all the groups.
groups = []
logger.info("[group_ownership] Retrieving groups information. Total pages: [%s]", total_pages)
for page in range(1, int(total_pages) + 1):
  logger.info("[group_ownership] Processing page %i out of %s", page, total_pages)
  resp = requests.get(base_url + "/groups?per_page=100&page=" + str(page), headers=api_sudo_token)
  groups_page = resp.json()
  groups = groups + groups_page

logger.info("[group_ownership] Retrieving group members, it may take a while")
for group in groups:
  resp = requests.get(base_url + "/groups/" + str(group['id']) + "/members", headers=api_sudo_token)
  groups_members = resp.json()
  group['members'] = groups_members

logger.info("[group_ownership] Finished retrieving group members")
# Print the JSON directly so we can redirect STDOUT to a file and upload it afterwards to S3
print(groups)