import os
import requests
import traceback
import logging
import sys
import argparse
import boto3
from botocore.client import Config

parser = argparse.ArgumentParser()
parser.add_argument("-s3url", "--s3_url", required=True)
parser.add_argument("-s3key", "--s3_access_key", required=True)
parser.add_argument("-s3secret", "--s3_secret_access_key", required=True)
parser.add_argument("-s3bucket", "--s3_bucket", required=True)
parser.add_argument("-sourcefilepath", "--source_file_path", required=True)
parser.add_argument("-sourcefilename", "--source_file_name", required=True)
parser.add_argument("-destinationpath", "--destination_path", required=True)
args = parser.parse_args()

# Force logger to print through stderr and add default format to messages
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stderr)
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

# Retrieve S3 variables, if they exist
s3_url = args.s3_url
s3_access_key = args.s3_access_key
s3_secret_access_key = args.s3_secret_access_key
s3_bucket = args.s3_bucket
source_file_path = args.source_file_path
source_file_name = args.source_file_name
destination_path = args.destination_path

# Finally, upload the results to S3 as for future reference
logger.info("[group_ownership] Uploading logging results to S3 bucket [%s]", s3_bucket)
s3 = boto3.resource('s3',
                    endpoint_url=s3_url,
                    aws_access_key_id=s3_access_key,
                    aws_secret_access_key=s3_secret_access_key,
                    config=Config(signature_version='s3v4'),
                    region_name='us-east-1')

s3.Bucket(s3_bucket).upload_file(source_file_path, os.path.join(destination_path, source_file_name))

logger.info("[group_ownership] GitLab ownership audit finished")